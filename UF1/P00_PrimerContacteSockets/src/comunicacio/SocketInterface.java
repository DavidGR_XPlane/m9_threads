package comunicacio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import exceptions.ProtocolException;

public class SocketInterface implements SocketComunicacio {

	@SuppressWarnings("unused")
	private boolean verbose = true;
	private PrintWriter out;
	private BufferedReader in;
	private BufferedReader systemIn;

	public SocketInterface(Socket socket) {
		super();
		try {
			this.out = new PrintWriter(socket.getOutputStream());
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.systemIn = new BufferedReader(new InputStreamReader(System.in));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SocketInterface(Socket socket, boolean verbose) {
		super();
		this.verbose = verbose;
		try {
			this.out = new PrintWriter(socket.getOutputStream());
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.systemIn = new BufferedReader(new InputStreamReader(System.in));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void send(String text) {
		out.println(text);
		System.out.println("Com a client envio: " + text);
	}

	@Override
	public String receive() {
		try {
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String systemRead() throws IOException {
		return systemIn.readLine();
	}

	@Override
	public void receive(String esperat) {
		try {
			String rebut = in.readLine();
			if (rebut != null) {
				if (!rebut.equals(esperat)) {
					throw new ProtocolException("Rebut: " + rebut + " Esperat: " + esperat);
				} else {
					System.out.println("Rebut: " + rebut + " Esperat: " + esperat);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			systemIn.close();
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String sendReceive(String text) {
		out.println(text);
		return receive();
	}

	public void sendReceive(String send, String receive) {
		send(send);
		receive(receive);
	}

}
