package comunicacio;

public interface SocketComunicacio {

	public void send(String text);

	public String receive();

	public void receive(String text);

	public void close();
	
	public String sendReceive(String text);

}
