package server_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import comunicacio.SocketInterface;

public class ServerAct2 {

	static final int portAct2 = 10001;

	public static void main(String[] args) {

		try {
			ServerSocket servSocket = new ServerSocket(portAct2);
			Socket clientSocket = servSocket.accept();
			SocketInterface sockInterface = new SocketInterface(clientSocket);
			System.out.println("WELCOME");
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String userInput;
//			while () {
//				userInput = sockInterface.receive();
//			}
			System.out.println("KTHXBYE");
			in.close();
			servSocket.close();
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}