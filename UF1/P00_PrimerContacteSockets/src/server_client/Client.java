package server_client;

import java.io.IOException;
import java.net.Socket;

import comunicacio.SocketInterface;

public class Client {

	public static void main(String[] args) {
		final String hostName = "localhost";
		try {
			Socket clientSocket = new Socket(hostName, Server.port);
			SocketInterface sockInterface = new SocketInterface(clientSocket);

			sockInterface.send("OH HI");
			sockInterface.send("BBYE");

			clientSocket.close();
			sockInterface.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}