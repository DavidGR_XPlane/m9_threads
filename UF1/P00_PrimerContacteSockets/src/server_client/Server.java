package server_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	static final int port = 5000;

	public static void main(String[] args) {

		try {
			ServerSocket servSocket = new ServerSocket(port);
			Socket clientSocket = servSocket.accept();

			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader inSock = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			System.out.println("WELCOME");

			String userInput;
			System.out.println("pre while");
			while ((userInput = inSock.readLine()) != null) {
				System.out.println("post while");
				if (!userInput.equals("BBYE")) {
					System.out.println("Rebut: " + userInput);
					out.println(userInput);
				}
				break;
			}

			servSocket.close();
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}