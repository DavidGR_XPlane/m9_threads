package Server;

import java.net.Socket;

import Comunicacio.Bytes;
import Comunicacio.ComunicacioSocket;

public class ClientHandler implements Runnable {
	public String nick;
	ComunicacioSocket comunicacioSocket;
	public byte bala;

	public ClientHandler(Socket socket) {
		System.out.println("A");
		this.comunicacioSocket = new ComunicacioSocket(socket);
		System.out.println("B");
	}

	@Override
	public void run() {
		salutacio();
		rebreNomClient();
		entrarLobby();
	}

	private void salutacio() {
		comunicacioSocket.sendReceive(Bytes.S_BENVINGUT, Bytes.ACK);
	}

	private void rebreNomClient() {
		String nick = comunicacioSocket.receive();
		System.out.println("Nick: " + nick);
//		while (!nickValid(nick)) {
//			System.out.println("El nick està en ús...");
//			comunicacioSocket.sendByte(Bytes.S_NICK_EN_US);
//			nick = comunicacioSocket.receive();
//		}
		this.nick = "client " + Server.clientHandlerId;
		comunicacioSocket.sendByte(Bytes.ACK);
	}

	private void entrarLobby() {
		System.out.println("CLIENT HANDLER - ENTRAR_LOBBY()");
//		if (Server.game.semaphore.availablePermits() > 0 && !Server.gameGoing) {
//			try {
//				Server.game.semaphore.acquire();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//
//		}		
		Server.game.entrarLlistaEspera(this);
		entrarPartida();
	}

//	private boolean nickValid(String name) {
//		for (ClientHandler handler : Server.game.esperantLobby) {
//			if (handler != this && nick.equals(handler.nick)) {
//				return false;
//			}
//		}
//		return true;
//	}

	private void entrarPartida() {
		while (true) {
			synchronized (Server.game.comencarPartida) {
				try {
					Server.game.comencarPartida.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (Server.game.intentarEntrar()) {
				comunicacioSocket.sendByte(Bytes.S_ESTAS_DINS);
				Server.game.entrarLlistaJugant(this);
				break;
			} else {
				comunicacioSocket.sendByte(Bytes.S_EN_CURS);
			}
		}
		jugar();
	}

	private void jugar() {
		System.out.println("JUGAR CLIENT HANDLER");
		boolean jugant = true;
		while (jugant) {
			synchronized (this) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			switch (bala) {
			case Bytes.S_BALA:
				comunicacioSocket.sendByte(Bytes.S_BALA);
				jugant = false;
				break;
			case Bytes.S_NO_BALA:
				comunicacioSocket.sendByte(Bytes.S_NO_BALA);
				break;
			case Bytes.S_FINALISTA:
				comunicacioSocket.sendByte(Bytes.S_FINALISTA);
				jugant = false;
				break;
			}
			comunicacioSocket.receiveByte(Bytes.ACK);
		}
		System.out.println("He acabat de jugar " + this.nick);
		synchronized (Server.game.acabarPartida) {
			try {
				Server.game.acabarPartida.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			comunicacioSocket.sendArray(Server.game.getNoms());
		}
		fiPartida();
	}

	private void fiPartida() {
		comunicacioSocket.sendByte(Bytes.S_CONTINUAR);
		byte resposta = comunicacioSocket.receiveByte();
		switch (resposta) {
		case Bytes.C_SEGUIR:
			entrarLobby();
			break;
		default:
			System.out.println("El client " + this.nick + " no vol jugar més");
			comunicacioSocket.close();
			break;

		}
	}

	@Override
	public String toString() {
		return "ClientHandler [nick=" + nick + "]";
	}

}