package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Game.Game;

public class Server {

	public static ExecutorService executor = Executors.newCachedThreadPool();

	public static boolean inLobby = true;
	public static final Game game = new Game();
	public static int clientHandlerId = 0;

	public static void main(String[] args) {
		int portNumber = 5000;
		try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
			executor.execute(game);
			while (true) {
				Socket clientSocket = serverSocket.accept();
				clientHandlerId++;
				System.out.println("Client acceptat");
				ClientHandler clientHandler = new ClientHandler(clientSocket);
				executor.execute(clientHandler);
			}
		} catch (IOException e) {
			System.out.println(
					"Exception caught when trying to listen to port " + portNumber + " or listening for a connection");
			System.out.println(e.getMessage());
		}
	}
}