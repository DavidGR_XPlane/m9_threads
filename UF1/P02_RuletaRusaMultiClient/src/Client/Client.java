package Client;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

import Comunicacio.Bytes;
import Comunicacio.ComunicacioSocket;

public class Client {

	private String nick;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		ComunicacioSocket comunicacioSocket;
		/* 60047 */
		/* 10.1.86.25 */

		try {
			comunicacioSocket = new ComunicacioSocket(new Socket("localhost", 5000));
			salutacio(comunicacioSocket);
			escollirNom(comunicacioSocket);
			entrarPartida(comunicacioSocket);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void salutacio(ComunicacioSocket socket) {
		socket.receiveByte(Bytes.S_BENVINGUT);
		socket.sendByte(Bytes.ACK);
	}

	private static void escollirNom(ComunicacioSocket socket) {
		String nick;
		while (true) {
			System.out.println("Escull el teu nickname: ");
			nick = sc.nextLine();
			socket.send(nick);
			byte respostaNickname = socket.receiveByte();
			if (respostaNickname == Bytes.ACK) {
				System.out.println("Nick " + nick + " correcte.");
				break;
			} else if (respostaNickname == Bytes.S_NICK_EN_US) {
				System.out.println("El nick està en ús...");
			}
		}
	}

	private static void entrarPartida(ComunicacioSocket socket) {
		System.out.println("CLIENT - INTENTEM ENTREM PARTIDA");
		// mirar si es pot entrar a la partida
		while (true) {
			byte respostaEntremPartida = socket.receiveByte();
			if (respostaEntremPartida == Bytes.S_EN_CURS) {
				System.out.println("Partida en curs, no puc entrar.");
			} else if (respostaEntremPartida == Bytes.S_ESTAS_DINS) {
				socket.sendByte(Bytes.ACK);
				System.out.println("Entro a la partida.");
				break;
			}
		}
		jugar(socket);
	}

	public static void jugar(ComunicacioSocket socket) {
		System.out.println("CLIENT - JUGAR()");
		boolean playing = true;
		while (playing) {
			byte bala = socket.receiveByte();
			switch (bala) {
			case Bytes.S_BALA:
				socket.sendByte(Bytes.ACK);
				System.out.println("He rebut bala, deixo de jugar.");
				playing = false;
				break;
			case Bytes.S_NO_BALA:
				socket.sendByte(Bytes.ACK);
				System.out.println("No he rebut bala.");
				break;
			case Bytes.S_FINALISTA:
				socket.sendByte(Bytes.ACK);
				System.out.println("Soc finalista.");
				playing = false;
				break;
			default:
				System.out.println("Error en la partida.");
				playing = false;
				break;
			}
		}
		recomptePunts(socket);
	}

	private static void recomptePunts(ComunicacioSocket socket) {
		System.out.println("CLIENT - RECOMPTE_PUNTS()");
		@SuppressWarnings("unchecked")
		List<String> noms = (List<String>) socket.receiveArray();
		System.out.println(noms);
		socket.sendByte(Bytes.ACK);
		fiPartida(socket);
	}

	private static void fiPartida(ComunicacioSocket socket) {
		System.out.println("CLIENT - FIPARTIDA()");
		socket.receiveByte(Bytes.S_CONTINUAR);
		socket.sendByte(Bytes.ACK);
		System.out.println("Vols seguir jugant? (y/n)");
		String resposta = sc.nextLine();
		switch (resposta) {
		case "y":
			socket.sendByte(Bytes.C_SEGUIR);
			entrarPartida(socket);
			break;
		default:
			socket.sendByte(Bytes.C_PLEGAR);
			System.out.println("Com a client marxo.");
			socket.close();
			break;
		}
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

}