package Exceptions;

@SuppressWarnings("serial")
public class ProtocolException extends Exception {

	public ProtocolException(String exception) {
		super(exception);
	}

}
