package Game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import Comunicacio.Bytes;
import Server.ClientHandler;

public class Game implements Runnable {

	public Object comencarPartida = new Object();
	public Object acabarPartida = new Object();

	public static boolean gameGoing = false;
	private int lobbySize = 2;
	public Semaphore semaphore = new Semaphore(lobbySize);

	public List<String> noms = new ArrayList<>();
	public List<ClientHandler> esperantLobby = new ArrayList<>();
	public List<ClientHandler> jugant = new ArrayList<>();
	public List<ClientHandler> morts = new ArrayList<>();

	@Override
	public void run() {
		while (true) {
			reiniciarPartida();
			System.out.println("Esperant suficients jugadors...");
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (esperantLobby.size() >= lobbySize) {
					synchronized (comencarPartida) {
						comencarPartida.notifyAll();
					}
					break;
				}
			}
			System.out.println("Els jugador estan entrant...");
			while (true) {
				try {
					Thread.sleep(1000);

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (jugant.size() >= lobbySize)
					break;
			}
			// cada ronda
			System.out.println("Comenca la partida...");
			while (jugant.size() > 1) {
				boolean[] cargador = nouCargador();
				for (int i = 0; i < jugant.size(); i++) {
					enviarBala(jugant.get(i), cargador[i]);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
					}
				}
				for (ClientHandler clientMort : morts) {
					jugant.remove(clientMort);
				}
			}
			for (ClientHandler ch : jugant) {
				ch.bala = Bytes.S_FINALISTA;
				noms.add(ch.nick);
				synchronized (ch) {
					ch.notify();
				}
			}
			System.out.println("Acaba la partida...");
			synchronized (acabarPartida) {
				acabarPartida.notifyAll();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void enviarBala(ClientHandler client, boolean bala) {
		System.out.println("Enviar bala a: " + client.nick);
		if (bala) {
			client.bala = Bytes.S_BALA;
			noms.add(client.nick);
			morts.add(client);
		} else {
			client.bala = Bytes.S_NO_BALA;
		}
		synchronized (client) {
			client.notify();
		}
	}

	public synchronized void entrarLlistaEspera(ClientHandler client) {
		esperantLobby.add(client);
	}

	public synchronized void entrarLlistaJugant(ClientHandler client) {
		esperantLobby.remove(client);
		jugant.add(client);
	}

	public synchronized boolean intentarEntrar() {
		return semaphore.tryAcquire();
	}

	private boolean[] nouCargador() {
		Random r = new Random();
		boolean[] cargador = new boolean[jugant.size()];
		cargador[r.nextInt(jugant.size())] = true;
		return cargador;
	}

	private void reiniciarPartida() {
		jugant.clear();
		noms.clear();
		morts.clear();
		this.semaphore = new Semaphore(lobbySize);
	}

	public synchronized List<String> getNoms() {
		return this.noms;
	}

}
