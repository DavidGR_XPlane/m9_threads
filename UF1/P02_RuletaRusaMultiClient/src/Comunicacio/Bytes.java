package Comunicacio;

public class Bytes {

	public static final byte ACK = 0b00000001;
	public static final byte S_BENVINGUT = 0b00000010;
	public static final byte S_NICK_EN_US = 0b00000011;
	public static final byte S_ESTAS_DINS = 0b00000100;
	public static final byte S_EN_CURS = 0b00000101;
	public static final byte S_NO_BALA = 0b00000110;
	public static final byte S_BALA = 0b00000111;
	public static final byte S_FINALISTA = 0b00001000;
	public static final byte S_CONTINUAR = 0b00001001;
	public static final byte C_SEGUIR = 0b00001010;
	public static final byte C_PLEGAR = 0b00001011;

	public String msgToString(byte message) {
		return switch (message) {
		case ACK -> "ACK";
		case S_BENVINGUT -> "S_BENVINGUT";
		case S_NICK_EN_US -> "S_NICK_EN_US";
		case S_ESTAS_DINS -> "S_ESTAS_DINS";
		case S_EN_CURS -> "S_EN_CURS";
		case S_NO_BALA -> "S_NO_BALA";
		case S_BALA -> "S_BALA";
		case S_FINALISTA -> "S_FINALISTA";
		case S_CONTINUAR -> "S_CONTINUAR";
		case C_SEGUIR -> "C_SEGUIR";
		case C_PLEGAR -> "C_PLEGAR";
		default -> "NO CONEGUT";
		};
	}

}
