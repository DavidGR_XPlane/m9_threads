package Comunicacio;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import Exceptions.ProtocolException;

public class ComunicacioSocket implements ISocketComunicacio {

	@SuppressWarnings("unused")
	private boolean verbose = true;
	private PrintWriter out;
	private BufferedReader in;
	private ObjectInputStream oin;
	private ObjectOutputStream oos;
	private DataOutputStream dos;
	private DataInputStream dis;
	private Bytes byteTranslator = new Bytes();

	public ComunicacioSocket(Socket socket) {
		super();
		try {
			this.dos = new DataOutputStream(socket.getOutputStream());
			this.dis = new DataInputStream(socket.getInputStream());
			this.oos = new ObjectOutputStream(socket.getOutputStream());
			this.oin = new ObjectInputStream(socket.getInputStream());
			this.out = new PrintWriter(socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendByte(byte text) {
		try {
			dos.writeByte(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Envio: " + byteTranslator.msgToString(text));
	}

	public byte receiveByte() {
		try {
			return (byte) dis.readByte();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void receiveByte(byte esperat) {
		try {
			System.out.println("antes");
			byte rebut = dis.readByte();
			System.out.println("RECEIVE");
			if (rebut != esperat)
				throw new ProtocolException("Rebut: " + byteTranslator.msgToString(rebut) + " Esperat: "
						+ byteTranslator.msgToString(esperat));
			else
				System.out.println("Rebut: " + byteTranslator.msgToString(rebut) + " Esperat: "
						+ byteTranslator.msgToString(esperat));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}

	public void sendReceive(byte send, byte receive) {
		sendByte(send);
		receiveByte(receive);
	}

	public void receiveSend(byte receive, byte send) {
		receiveByte(receive);
		sendByte(send);
	}

	/* --------------------------------------------- */
	/* --------------------------------------------- */
	/* --------------------------------------------- */

	/* --------------------------------------------- */
	/* --------------------------------------------- */
	/* --------------------------------------------- */

	/* --------------------------------------------- */
	/* --------------------------------------------- */
	/* --------------------------------------------- */

	@Override
	public void send(String text) {
		out.println(text);
		System.out.println("Envio: " + text);
	}

	public void sendArray(Object obj) {
		try {
			oos.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Envio: " + obj);
	}

	@Override
	public String receive() {
		try {
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object receiveArray() {
		try {
			return oin.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void receiveSend(String send) {
		simpleReceive();
		send(send);
	}

//	public String systemRead() throws IOException {
//		return systemIn.readLine();
//	}

	public void simpleReceive() {
		try {
			System.out.println("Simple receive");
			String rebut = in.readLine();
			System.out.println("Server-Rebut: " + rebut);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public String readConsole() { try { return systemIn.readLine(); } catch
	 * (IOException e) { e.printStackTrace(); } return ""; }
	 */

	@Override
	public void receive(String esperat) {
		try {
			System.out.println("antes");
			String rebut = in.readLine();
			System.out.println("RECEIVE");
			if (rebut != null) {
				if (!rebut.equals(esperat))
					throw new ProtocolException("Rebut: " + rebut + " Esperat: " + esperat);
				else
					System.out.println("Rebut: " + rebut + " Esperat: " + esperat);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			// systemIn.close();
			out.close();
			in.close();
			dos.close();
			dis.close();
			oos.close();
			oin.close();
			/* systemIn.close(); */
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String sendReceive(String text) {
		try {
			dos.writeUTF(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return receive();
	}

	public void sendReceive(String send, String receive) {
		send(send);
		receive(receive);
	}

}
