package comunicacio;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import exceptions.ProtocolException;

public class SocketClass implements SocketComunicacio {

	@SuppressWarnings("unused")
	private boolean verbose = true;
	private PrintWriter out;
	private BufferedReader in;
	private ObjectInputStream oin;
	private ObjectOutputStream oos;
	private DataOutputStream dos;
	private DataInputStream dis;
	private BufferedReader systemIn;

	public SocketClass(Socket socket) {
		super();
		try {
			this.out = new PrintWriter(socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.oin = new ObjectInputStream(socket.getInputStream());
			this.oos = new ObjectOutputStream(socket.getOutputStream());
			this.dos = new DataOutputStream(socket.getOutputStream());
			this.dis = new DataInputStream(socket.getInputStream());
			this.systemIn = new BufferedReader(new InputStreamReader(System.in));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void send(String text) {
		out.println(text);
		System.out.println("Com a client envio: " + text);
	}

	public void sendArray(Object obj) throws IOException {
		oos.writeObject(obj);
		System.out.println("Com a client envio: " + obj);
	}

	@Override
	public String receive() {
		try {
			return in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object receiveArray() {
		try {
			return oin.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void receiveSend(String receive, String send) {
		simpleReceive();
		send(send);
	}

//	public String systemRead() throws IOException {
//		return systemIn.readLine();
//	}

	public void simpleReceive() {
		try {
			System.out.println("Simple receive");
			String rebut = in.readLine();
			System.out.println("Server-Rebut: " + rebut);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String readConsole() {
		try {
			return systemIn.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void receive(String esperat) {
		try {
			System.out.println("antes");
			String rebut = in.readLine();
			System.out.println("RECEIVE");
			if (rebut != null) {
				if (!rebut.equals(esperat))
					throw new ProtocolException("Rebut: " + rebut + " Esperat: " + esperat);
				else
					System.out.println("Rebut: " + rebut + " Esperat: " + esperat);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			// systemIn.close();
			out.close();
			in.close();
			dos.close();
			dis.close();
			oos.close();
			oin.close();
			systemIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String sendReceive(String text) {
		try {
			dos.writeUTF(text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return receive();
	}

	public void sendReceive(String send, String receive) {
		send(send);
		receive(receive);
	}

}
