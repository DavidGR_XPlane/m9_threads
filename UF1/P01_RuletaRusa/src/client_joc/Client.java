package client_joc;

import java.io.IOException;
import java.net.Socket;

import comunicacio.SocketClass;

public class Client {

	static boolean gameGoing;

	public static void main(String[] args) {
		try {
			gameGoing = true;
			Socket clientSocket = new Socket("localhost", 60047);
			// SocketClass socketInterface = new SocketClass(new Socket("10.1.86.16",
			// 5000));
			SocketClass socketInterface = new SocketClass(clientSocket);
			System.out.println("pre cyka");
			socketInterface.receiveSend("CYKA BLYATT", "ACK");
			System.out.println("pre while");
			while (gameGoing) {
				System.out.println("entro while");
				socketInterface.receiveSend("QUIERO JUGAR A UN JUEGO", "ACK");
				int[] balas = (int[]) socketInterface.receiveArray();
				socketInterface.send("ACK");
				socketInterface.sendReceive(jugar(balas), "ACK");
				socketInterface.receiveSend("NO HAY HUEVOS", choose(socketInterface));
			}
			socketInterface.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String jugar(int[] cargador) {
		boolean muerto = false;
		int ronda = 1;
		for (int i = 0; i < cargador.length - 1; i++) {
			if (i % 2 == 0 && cargador[i] == 1) {
				muerto = true;
				break;
			}
			ronda++;
		}
		if (muerto)
			return "MUELTO VIVO " + ronda;
		return "VIVO " + ronda;
	}

	private static String choose(SocketClass socket) {
		System.out.println("1 - seguir");
		System.out.println("2 - el que la saca la usa");

		String opcio = socket.readConsole().trim();

		switch (opcio) {
		case "1":
			return "EL QUE LA SACA LA USA";
		default:
			gameGoing = false;
			return "SOY UN PARGUELA";
		}
	}

}