package server_joc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Server {

	static boolean jugando;

	public static void main(String[] args) {

		try {

			ServerSocket serverSocket = new ServerSocket(60047);

			while (true) {
				jugando = true;
				System.out.println("Waiting...");
				Socket client_socket = serverSocket.accept();

				PrintWriter out = new PrintWriter(client_socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(client_socket.getInputStream()));
				ObjectOutputStream oos = new ObjectOutputStream(client_socket.getOutputStream());
				System.out.println("Client connectat...");

				out.println("CYKA BLYATT");
				System.out.println("CYKA");
				System.out.println(in.readLine()); // ACK
				System.out.println("ACK");
				while (jugando) {
					out.println("QUIERO JUGAR A UN JUEGO");
					System.out.println(in.readLine()); // ACK
					oos.writeObject(generarRuleta());
					System.out.println(in.readLine()); // ACK
					System.out.println(in.readLine()); // VIVO O MUERTO
					out.println("ACK");
					out.println("NO HAY HUEVOS?");
					switch (in.readLine()) {
					case "SOY UN PARGUELA":
						jugando = false;
						out.println("KURWA");
						out.close();
						in.close();
						oos.close();
						client_socket.close();
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static int[] generarRuleta() {
		Random r = new Random();
		int[] ruleta = new int[6];
		ruleta[r.nextInt(0, ruleta.length)] = 1;
		return ruleta;
	}
}
