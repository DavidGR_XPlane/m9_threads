package Test;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class PilasColas {

	public static void main(String[] args) {

		Queue<String> cola = new LinkedList<String>();
		cola.offer("hola"); // afegir última posició
		cola.peek(); // retorna el primer element de la cua
		cola.poll(); // retorna el primer element i el borra de la cua
		cola.remove(""); // igual que poll, però, si es buida, excepció
		cola.clear(); // buida la cua

		Deque<String> pila = new LinkedList<String>();
		pila.offerFirst(""); // afegir a primera posició
		pila.offerLast(""); // afegir a última posició
		pila.offer(""); // = offerLast()
		pila.peekFirst(); // retorna el primer element de la pila
		pila.peekLast(); // retorna l'últim element de la pila
		pila.peek(); // = peekFirst()
		pila.remove(); // igual que poll, però, si es buida, excepció
		pila.clear(); // buida la cua
//
		pila.offer("hola");
		pila.offer("mig");
		pila.offer("adeu");

		System.out.println(pila.peek()); // hola
		System.out.println(pila.peekFirst()); // hola
		System.out.println(pila.peekLast()); // adeu

		pila.clear();
		pila.offerFirst("hola");
		pila.offerFirst("adeu");

		System.out.println();

		System.out.println(pila.peek()); // adeu
		System.out.println(pila.peekFirst()); // adeu
		System.out.println(pila.peekLast()); // hola

	}

}
