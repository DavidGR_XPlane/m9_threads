package Main;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import MichiAvançat.Michilover;
import MichiAvançat.Michismart;
import MichiBasic.Michibi;
import MichiBasic.Michonk;

public class GameController {

	public static boolean gameGoing;
	public static int michiTokens;

	public static int michibi;
	public static int michonk;
	public static int michilover;
	public static int michismart;

	public static InputController inputController = new InputController();
	public static ExecutorService inputExecutor = Executors.newCachedThreadPool();
	public static ExecutorService michiExecutor = Executors.newCachedThreadPool();
	public static CountDownLatch cdlLover = new CountDownLatch(5);
	public static Semaphore semaphoreLover = new Semaphore(2);

	public static void main(String[] args) {
		initGame();
	}

	private static void initGame() {
		michiTokens = 0;
		michibi = 0;
		michonk = 0;
		michilover = 0;
		michismart = 0;
		gameGoing = true;
		inputExecutor.execute(inputController);
		printGUI();
	}

	private static void printGUI() {
		System.out.println("Michibis: " + michibi);
		System.out.println("Michonks: " + michonk);
		System.out.println("Michilover: " + michilover);
		System.out.println("Michismart: " + michismart);
		System.out.println("Michitokens actuals: " + michiTokens);
		System.out.println("Digues que vols fer...");
		printInput();
	}

	private static void printInput() {
		System.out.println("0- Quit Game");
		System.out.println("1- Action1");
		System.out.println("2- Action2");
		System.out.println("3- Action3");
		System.out.println("4- Action4");
	}

	public synchronized static void modificarMichitokens(int tokens) {
		michiTokens += tokens;
		printGUI();
	}

	/**
	 * 
	 * Crear Michibis
	 * 
	 */
	public static void action1() {
		if (michiTokens < 5) {
			System.out.println("No tens suficients Michitokens per comprar un Michibi!");
			return;
		}
		michibi++;
		System.out.println("Afegint un MICHIBI!");
		modificarMichitokens(-5);
		michiExecutor.execute(new Michibi());
	}

	/**
	 * 
	 * Crear Michonks
	 * 
	 */
	public static void action2() {
		if (michiTokens < 20) {
			System.out.println("No tens suficients Michitokens per comprar un Michonk!");
			return;
		}
		michonk++;
		System.out.println("Afegint un MICHONK!");
		modificarMichitokens(-20);
		michiExecutor.execute(new Michonk());
	}

	/**
	 * 
	 * Crear Michilovers
	 * 
	 */
	public static void action3() {
		if (michiTokens < 10) {
			System.out.println("No tens suficients Michitokens per comprar un Michilover!");
			return;
		}
		michilover++;
		if (cdlLover.getCount() == 0) {
			michilover = 1;
			cdlLover = new CountDownLatch(5);
		}
		cdlLover.countDown();
		System.out.println("Afegint un MICHILOVER");
		michiExecutor.execute(new Michilover());
		modificarMichitokens(-10);
	}

	/**
	 * 
	 * Crear Michismarts
	 * 
	 */
	public static void action4() {
		if (michiTokens < 100) {
			System.out.println("No tens suficients Michitokens per comprar un Michismart!");
			return;
		}

		System.out.println("Afegint un MICHISMART");
		michiExecutor.execute(new Michismart());
		GameController.michismart++;
		modificarMichitokens(-100);
	}

	public static void quitGame() {
		gameGoing = false;
		michiExecutor.shutdown();
		inputExecutor.shutdown();
		try {
			if (!michiExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
				System.out.println("Han passat 5 segons i finalitzem els fils");
				michiExecutor.shutdownNow();
				inputExecutor.shutdownNow();
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
		System.out.println("Tancant joc...");
	}
}