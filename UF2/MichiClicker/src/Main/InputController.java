package Main;

import java.util.Scanner;

public class InputController implements Runnable {
	@Override
	public void run() {
		Scanner sc = new Scanner(System.in);
		while (GameController.gameGoing) {
			String input = sc.nextLine();
			switch (input) {
			case "0":
				GameController.quitGame();
				break;
			case "1":
				//michibi
				GameController.action1();
				break;
			case "2":
				//michonk
				GameController.action2();
				break;
			case "3":
				//michilover
				GameController.action3();
				break;
			case "4":
				//michismart
				GameController.action4();
				break;
			default:
				System.out.println("Sumant 1 Michitoken!");
				GameController.modificarMichitokens(1);
				break;
			}
		}
		sc.close();
	}
}
