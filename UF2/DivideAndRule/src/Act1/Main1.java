package Act1;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main1 {
	public static void main(String[] args) {
		Random r = new Random();
		int n = 1000000;
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			int num = r.nextInt();
			nums.add(num);
		}
		long listDone = System.currentTimeMillis() * 1000000;	
		ExecutorService executor = Executors.newFixedThreadPool(2);
		Callable1 callable = new Callable1(nums);
		// executor.execute(callable);
		Future<int[]> result = executor.submit(callable);
		try {
			System.out.println("MIN NUMBER " + result.get()[0]);
			System.out.println("MAX NUMBER " + result.get()[1]);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long allDone = System.currentTimeMillis() * 1000000;
		System.out.println("TOTAL TIME: " + (allDone - listDone) + " NANOSECONDS");
		executor.shutdown();
		try {
			if (!executor.awaitTermination(4, TimeUnit.SECONDS)) {
				System.out.println("Han passat 3 segons i finalitzem els fils");
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
	}
}
