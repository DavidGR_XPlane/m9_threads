package Act3;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

public class Callable3 implements Callable<List<Integer>> {

	public List<Integer> arr;

	public Callable3(List<Integer> list) {
		this.arr = list;
	}

	@Override
	public List<Integer> call() throws Exception {
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i) < min) {
				min = arr.get(i);
			}
			if (arr.get(i) > max) {
				max = arr.get(i);
			}
		}
		return Arrays.asList(min, max);
	}

}
