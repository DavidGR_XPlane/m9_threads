package Act3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main3 {

	public static void main(String[] args) {
		Random r = new Random();
		Scanner sc = new Scanner(System.in);
		System.out.println("Quantitat de nums a la llista: ");
		int n = sc.nextInt();
		System.out.println("Quantitat de threads: ");
		int threads = sc.nextInt();
		sc.close();
		ArrayList<Future<List<Integer>>> result = new ArrayList<Future<List<Integer>>>();
		List<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			int num = r.nextInt(0, Integer.MAX_VALUE);
			nums.add(num);
		}
		long listDone = System.currentTimeMillis() * 1000000;
		ExecutorService executor = Executors.newFixedThreadPool(threads);
		for (int j = 0; j < threads; j++) {
			result.add(executor.submit(new Callable3(nums.subList((n / threads) * j, n / threads * (j + 1)))));
		}
		executor.shutdown();
		try {
			if (!executor.awaitTermination(4, TimeUnit.SECONDS)) {
				System.out.println("Han passat 3 segons i finalitzem els fils");
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
		System.out.println();
		try {
			int min_M = Integer.MAX_VALUE;
			int max_M = Integer.MIN_VALUE;
			for (int i = 0; i < result.size(); i++) {
				int min_T = result.get(i).get().get(0);
				int max_T = result.get(i).get().get(1);
				System.out.println("Thread " + i + "MIN: " + min_T);
				System.out.println("Thread " + i + "MAX: " + max_T);
				if (max_T > max_M)
					max_M = max_T;
				if (min_T < min_M)
					min_M = min_T;
			}
			System.out.println("MAX TOTAL " + max_M);
			System.out.println("MAX TOTAL " + min_M);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		long allDone = System.currentTimeMillis() * 1000000;
		System.out.println("TOTAL TIME: " + (allDone - listDone) + " NANOSECONDS");
	}
}
