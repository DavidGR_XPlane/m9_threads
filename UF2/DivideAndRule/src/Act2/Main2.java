package Act2;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main2 {

	public static void main(String[] args) {
		Random r = new Random();
		int n = 1000000;
		ArrayList<Future<int[]>> result = new ArrayList<Future<int[]>>();
		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			int num = r.nextInt();
			nums.add(num);
		}
		long listDone = System.currentTimeMillis() * 1000000;
		ExecutorService executor = Executors.newFixedThreadPool(2);
		Callable2 callable = new Callable2(new ArrayList<Integer>(nums.subList(0, nums.size() / 2)));
		Callable2 callable2 = new Callable2(new ArrayList<Integer>(nums.subList((nums.size() / 2) + 1, nums.size())));
		ArrayList<Callable2> callables = new ArrayList<Callable2>();
		ArrayList<Integer> arrFinal = new ArrayList<Integer>();
		callables.add(callable);
		callables.add(callable2);
//		for (int j = 0; j < callables.size(); j++) {
//			result.add(executor.submit(new Callable2(
//					new ArrayList<Integer>(nums.subList(j / callables.size(), n / callables.size() + 1)))));
//		}
		for (int j = 0; j < callables.size(); j++) {
			result.add((executor.submit(callables.get(j))));
		}
		System.out.println();

		try {
			int min_M = Integer.MAX_VALUE;
			int max_M = Integer.MIN_VALUE;
			for (int i = 0; i < result.size(); i++) {
				int min_T = result.get(i).get()[0];
				int max_T = result.get(i).get()[1];
				System.out.println("Thread " + i + "MIN: " + min_T);
				System.out.println("Thread " + i + "MAX: " + max_T);
				if (max_T > max_M)
					max_M = max_T;

				if (min_T < min_M)
					min_M = min_T;
			}
			System.out.println("MAX TOTAL " + max_M);
			System.out.println("MAX TOTAL " + min_M);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		try {
			if (!executor.awaitTermination(4, TimeUnit.SECONDS)) {
				System.out.println("Han passat 3 segons i finalitzem els fils");
				executor.shutdownNow();
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
		long allDone = System.currentTimeMillis() * 1000000;
		System.out.println("TOTAL TIME: " + (allDone - listDone) + " NANOSECONDS");
		// System.out.println(result);
	}
	
}
