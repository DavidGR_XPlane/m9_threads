package Act2;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Callable2 implements Callable<int[]> {

	public ArrayList<Integer> arr;

	public Callable2(ArrayList<Integer> arr) {
		this.arr = arr;
	}

	@Override
	public int[] call() throws Exception {
		int[] hola = new int[2];
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		//int[] arrFinal = new int[2];
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i) < min) {
				min = arr.get(i);
			}
			if (arr.get(i) > max) {
				max = arr.get(i);
			}
		}
		//nums.add(min);
		hola[0] = min;
		//nums.add(max);
		hola[1] = max;
		return hola;
	}

}
