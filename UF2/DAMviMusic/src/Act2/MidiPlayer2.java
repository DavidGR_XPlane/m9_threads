package Act2;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

public final class MidiPlayer2 {

	private static final int VELOCITY = 75;
	private static Synthesizer midiSynth;
	private static MidiChannel[] mChannels;

	static {
		try {
			midiSynth = MidiSystem.getSynthesizer();
			midiSynth.open();

			// get and load default instrument and channel lists
			Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
			mChannels = midiSynth.getChannels();

			midiSynth.loadInstrument(instr[20]);// load an instrument

		} catch (MidiUnavailableException e) {
			throw new RuntimeException(e);
		}
	}

	public static Instrument[] getInstruments() {
		return midiSynth.getDefaultSoundbank().getInstruments();
	}

	public static boolean setInstrument(Instrument instrument) {
		return midiSynth.loadInstrument(instrument);
	}

	public static void play(int channel, Note2... notes) {
		for (Note2 note : notes) {
			if (note.getFrequency() != Note2.Frequency.SILENCE)
				mChannels[channel].noteOn(note.getFrequency(), VELOCITY);
		}
	}

	public static void play(Note2... notes) {
		play(0, notes);
	}

	public static void stop(int channel, Note2... notes) {
		for (Note2 note : notes) {
			if (note.getFrequency() != Note2.Frequency.SILENCE)
				mChannels[channel].noteOff(note.getFrequency(), VELOCITY);
		}
	}

	public static void stop(Note2... notes) {
		stop(0, notes);
	}
}