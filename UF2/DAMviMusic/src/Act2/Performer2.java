package Act2;

import java.util.concurrent.Callable;

public class Performer2 implements Callable<Note2> {

	Note2[] partitura;
	int canal;
	Conductor2 conductor;

	public Performer2(Note2[] partitura, int canal, Conductor2 conductor) {
		this.partitura = partitura;
		this.canal = canal;
		this.conductor = conductor;
	}

	@Override
	public Note2 call() throws Exception {

		for (int i = 0; i < partitura.length; i++) {
			MidiPlayer2.play(canal, partitura[i]);
			System.out.println("o" + canal);
			for (int j = 0; j < partitura[i].getDuration(); j++) {
				synchronized (conductor) {
					conductor.wait();	
				}
			}
			MidiPlayer2.stop(canal, partitura[i]);
		}
		return null;
	}
}
