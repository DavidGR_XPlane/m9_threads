package Act2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMusic2 {
	public static void main(String[] args) {
		Note2[] notesLead = { new Note2(Note2.Frequency.C4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D4, Note2.Duration.quarter), new Note2(Note2.Frequency.E4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G4, Note2.Duration.quarter), new Note2(Note2.Frequency.C5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G5, Note2.Duration.quarter), new Note2(Note2.Frequency.C6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D5, Note2.Duration.quarter), new Note2(Note2.Frequency.C5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G4, Note2.Duration.quarter), new Note2(Note2.Frequency.E4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D4, Note2.Duration.quarter),

				new Note2(Note2.Frequency.A3, Note2.Duration.quarter), new Note2(Note2.Frequency.B3, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C4, Note2.Duration.quarter), new Note2(Note2.Frequency.E4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A4, Note2.Duration.quarter), new Note2(Note2.Frequency.B4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C5, Note2.Duration.quarter), new Note2(Note2.Frequency.B4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A4, Note2.Duration.quarter), new Note2(Note2.Frequency.E4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.B3, Note2.Duration.quarter) };

		Note2[] notesRythm = { new Note2(Note2.Frequency.G5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A5, Note2.Duration.quarter), new Note2(Note2.Frequency.B5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D5, Note2.Duration.quarter), new Note2(Note2.Frequency.G6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A6, Note2.Duration.quarter), new Note2(Note2.Frequency.B6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D6, Note2.Duration.quarter), new Note2(Note2.Frequency.C7, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G6, Note2.Duration.quarter), new Note2(Note2.Frequency.E6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D6, Note2.Duration.quarter), new Note2(Note2.Frequency.C6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.G5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.D5, Note2.Duration.quarter),

				new Note2(Note2.Frequency.A4, Note2.Duration.quarter), new Note2(Note2.Frequency.B4, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A5, Note2.Duration.quarter), new Note2(Note2.Frequency.B5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C6, Note2.Duration.quarter), new Note2(Note2.Frequency.E6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A6, Note2.Duration.quarter), new Note2(Note2.Frequency.E6, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C6, Note2.Duration.quarter), new Note2(Note2.Frequency.B5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.A5, Note2.Duration.quarter), new Note2(Note2.Frequency.E5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.C5, Note2.Duration.quarter),
				new Note2(Note2.Frequency.B4, Note2.Duration.quarter) };

		/*
		 * Instrument[] instruments = MidiPlayer.getInstruments(); for(Instrument
		 * instrument: instruments) System.out.println(instrument.getName());
		 */

		ExecutorService executor = Executors.newCachedThreadPool();
		Conductor2 conductor = new Conductor2();
		Performer2 performer1 = new Performer2(notesLead, 0, conductor);
		Performer2 performer2 = new Performer2(notesRythm, 1, conductor);
		ArrayList<Performer2> performers = new ArrayList<Performer2>(Arrays.asList(performer1, performer2));

		for (Performer2 p : performers) {
			executor.submit(p);
		}
		executor.execute(conductor);

//		try {
//
//			for (int i = 0; i < notesLead.length; i++) {
//				// MidiPlayer.setInstrument(instruments[0]);
//
//				MidiPlayer.play(0, notesLead[i]);
//				MidiPlayer.play(1, notesRythm[i]);
//				Thread.sleep(250);
//				MidiPlayer.stop(0, notesLead[i]);
//				MidiPlayer.stop(1, notesRythm[i]);
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}

	}

}
