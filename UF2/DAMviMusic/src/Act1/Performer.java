package Act1;

import java.util.concurrent.Callable;

public class Performer implements Callable<Boolean> {

	Note[] partitura;
	boolean acabat;

	public Performer(Note[] partitura) {
		this.partitura = partitura;
		acabat = false;
	}

	public synchronized Boolean call() throws Exception {
		int tempo = (60000/60) / 16;
		for (int i = 0; i < partitura.length; i++) {
			MidiPlayer.play(0, partitura[i]);
			System.out.println("o");
			for (int j = 0; j < partitura[i].getDuration(); j++) {
				Thread.sleep(tempo);

			}
			MidiPlayer.stop(0, partitura[i]);
		}
		acabat = true;
		return acabat;
	}
}
