package Act2;

import java.util.ArrayList;
import java.util.Random;

public class TimerMain2 {

	/*
	 * Aprofitant el fil temporitzador anterior, implementa un nou programa on el
	 * fil principal crei un temporitzador cada mig segon fins a un total de 10
	 * temporitzadors. Cada un d’aquests fils temporitzadors han d’indicar el seu
	 * nom i quants segons li queden per acabar (per cada segon).
	 * 
	 * El fil principal ha d’esperar que tots els fils acabin abans d’acabar ell.
	 */

	public static void main(String[] args) throws InterruptedException {

		int secTemp;
		Random r = new Random();
		ArrayList<Thread> threadsList = new ArrayList<Thread>();
		
		for (int i = 0; i < 10; i++) {
			secTemp = r.nextInt(1, 11);
			ThreadTemporitzador2 thread = new ThreadTemporitzador2(secTemp);
			thread.nom = i;
			threadsList.add(thread);
			thread.start();
			Thread.sleep(500);
		}
		for (Thread thread : threadsList) {
			thread.join();
		}
		
		System.out.println("Final");
		
		/*
		 * thread.start(); thread.join(); System.out.println("FI");
		 */
	}

}
