package Act2;


public class ThreadTemporitzador2 extends Thread {
	public int seconds;
	public int nom;

	public ThreadTemporitzador2(int sec) {
		this.seconds = sec;
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		try {
			for (int i = seconds; i > 0; i--) {
				System.out.println("Soc " + this.nom + " i em queden " + i + " segons");
				this.sleep(1000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
