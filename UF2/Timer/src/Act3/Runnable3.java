package Act3;

import java.util.Random;

public class Runnable3 implements Runnable {

	int pId;
	int sec;
	int secFinal;

	public Runnable3(int pId, int sec) {
		this.pId = pId;
		this.sec = sec;
		this.secFinal = sec;
	}

	@Override
	public void run() {
		Random r = new Random();
		// s'indica la id del fil i el id del fil a la pool
		System.out.println("Fill: " + this.pId + "AGAFA el pool: " + Thread.currentThread().getName());
		try {
			for (int i = sec; i >= 0; i--) {
				secFinal--;
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			System.out.println("Fill: " + this.pId + " Es interromput: " + Thread.currentThread().getName() + " queden "
					+ secFinal + " segons ");
		}
		System.out.println("Fill: " + this.pId + "DEIXA el pool: " + Thread.currentThread().getName());
	}
}
