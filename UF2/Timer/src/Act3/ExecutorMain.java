package Act3;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorMain {

	public static void main(String[] args) {

		Random r = new Random();
		// executor service
		// és un gestor de threads

		// li indiques quants threads poden funcionar alhora,
		// però està limitat als threads del CPU
		ExecutorService executor = Executors.newFixedThreadPool(10);
		// el fil principal crea 10 temporitzadors
		for (int i = 0; i < 10; i++) {
			// creem els (10 en aquest cas) runnables per mandarli
			// al nostre executor
			// fem que començi l'executor
			executor.execute(new Runnable3(i, r.nextInt(0, 11)));
		}
		// el shutdown fa que l'executor no accepti més tasques,
		// és a dir, tanca la porta i no deixa passar a ningú més
		executor.shutdown();
		try {
			for (int i = 5; i >= 0; i--) {
				// l'await termination atura tots els threads
				// dins de l'executor durant el temps que s'indiqui
				executor.awaitTermination(1, TimeUnit.SECONDS);
				// es para 1 segón (el que l'hem indicat)
				System.out.println("Em queden " + i + " segons");
			}
			// fem que l'executor aturi totes les tasques
			// i les interrompi
			// shutdown: tancar porta, no entra ningú més
			// shutdownNow: parem totes les tasques del tot, hagin acabat o no
			executor.shutdownNow();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}