package Act1;

import java.util.Random;

public class TimerMain1 {

	/*
	 * Implementa un fil temporitzador el qual donada una quantitat de segons,
	 * esperarà els segons que li indiquis abans d’acabar i cada 1 segon dirà quants
	 * li queden. El fil principal ha d’inicialitzar el temporitzador a un temps
	 * aleatori (1~10), esperar que el temporitzador acabi i, aleshores, acabar ell.
	 */

	public static void main(String[] args) throws InterruptedException {
		int secTemp;
		Random r = new Random();
		secTemp = r.nextInt(1, 11);
		System.out.println(secTemp);
		ThreadTemporitzador1 thread = new ThreadTemporitzador1(secTemp);
		thread.start();
		thread.join();

		System.out.println("FI");
	}

}
