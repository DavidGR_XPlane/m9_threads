package Act1;

public class ThreadTemporitzador1 extends Thread {
	public int seconds;

	public ThreadTemporitzador1(int sec) {
		this.seconds = sec;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = seconds; i > 0; i--) {
			System.out.println("Em queden " + i + " segons");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
