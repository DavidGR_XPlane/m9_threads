package Examen;

import java.util.Random;
import java.util.concurrent.Callable;

public class Jugador implements Callable<int[]> {

	String nom;
	int puntuacio;
	boolean mort;

	public Jugador(String nom, int puntuacio) {
		this.nom = nom;
		this.puntuacio = puntuacio;
		this.mort = false;
	}

	@Override
	public int[] call() throws Exception {
		System.out.println("soc el jugador " + this.nom + " jugar");
		Random r = new Random();
		Thread.sleep(r.nextInt(1000, 3001));
		while (!mort && Main.gameGoing) {
			switch (r.nextInt(0, 3)) {
			case 0:
				morir();
				break;
			default:
				//
				break;
			}
			Thread.sleep(r.nextInt(1000, 3001));
		}
		Main.JugadorsActius.remove(this);
		int[] dadesJugador = new int[2];
		return dadesJugador;
	}

	@Override
	public String toString() {
		return "Jugador [nom=" + nom + ", puntuacio=" + puntuacio + "]";
	}

	private void morir() {
		mort = true;
		System.out.println(this.nom + " ha mort");
		Main.semaforPartida.release();
	}

}
