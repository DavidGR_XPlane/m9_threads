package Examen;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Main {

	public static boolean gameGoing;
	public static Semaphore semaforPartida = new Semaphore(5);

	public static Queue<Jugador> JugadorsEsperant = new LinkedList<Jugador>();
	public static Queue<Jugador> JugadorsActius = new LinkedList<Jugador>();

	public static void main(String[] args) {
		
		for (int i = 0; i < 15; i++) {
			
			JugadorsEsperant.offer(new Jugador("Jugador" + i, 0));
			
		}

		System.out.println("Comença la partida...");
		gameGoing = true;
		ExecutorService executorPartida = Executors.newCachedThreadPool();
		while (JugadorsEsperant.size() >= 1 && gameGoing == true) {
			while (semaforPartida.availablePermits() != 0) {
				Jugador nouJugador = JugadorsEsperant.poll();
				System.out.println("queden " + JugadorsEsperant.size() + " esperant");
				JugadorsActius.offer(nouJugador);
				try {
					semaforPartida.acquire();
					executorPartida.submit(nouJugador);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (JugadorsEsperant.size() == 0 && JugadorsActius.size() == 1) {
				gameGoing = false;
			}
		}
		executorPartida.shutdown();
		try {
			if (!executorPartida.awaitTermination(5, TimeUnit.SECONDS)) {
				System.out.println("Han passat 5 segons i finalitzem els fils");
				executorPartida.shutdownNow();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Jugador guanyador = JugadorsActius.poll();
		System.out.println(guanyador + " HA GUANYAT");
	}

}
