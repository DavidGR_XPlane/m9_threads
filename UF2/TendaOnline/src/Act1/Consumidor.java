package Act1;

import java.util.Random;

public class Consumidor implements Runnable {

	private BufferB buffer;

	public Consumidor(BufferB b) {
		this.buffer = b;
	}

	@Override
	public void run() {

		while (true) {
			try {
				Random r = new Random();
				String c = this.buffer.consumir();
				System.out.println("Consumim  l'avió " + c + " del buffer");
				Thread.sleep(r.nextInt(7) * 1000);
			} catch (Exception e) {

			}
		}

	}

}
