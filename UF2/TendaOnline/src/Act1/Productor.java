package Act1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Productor implements Runnable {

	private BufferB buffer;
	private final ArrayList<String> airbusProductes;

	public Productor(BufferB b) {
		this.airbusProductes = new ArrayList<String>(Arrays.asList("A318", "A319", "A320", "A321", "A330", "A340-300",
				"A340-500", "A340-600", "A350", "A380"));
		this.buffer = b;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Random r = new Random();
				String prod = airbusProductes.get(r.nextInt(0, airbusProductes.size() + 1));
				buffer.produir(prod);
				System.out.println("Depositem el l'avió " + prod + " al buffer");
				Thread.sleep(r.nextInt(5) * 1000);
			} catch (Exception e) {

			}
		}
	}
}