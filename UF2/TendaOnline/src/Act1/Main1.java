package Act1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main1 {

	public static void main(String[] args) {

		BufferB b = new BufferB(10);
		ExecutorService executor = Executors.newFixedThreadPool(2);
		executor.execute(new Productor(b));
		executor.execute(new Consumidor(b));
		
	}

}
