package Act1;

public class BufferB {

	private String[] buffer;
	private int seguent;
	private boolean estaBuida;
	private boolean estaPlena;

	public BufferB(int tamany) {
		this.buffer = new String[tamany];
		this.seguent = 0;
		this.estaBuida = true;
		this.estaPlena = false;
	}

	public synchronized void produir(String c) {
		while (this.estaPlena) {
			try {
				wait();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		buffer[seguent] = c;
		seguent++;
		this.estaBuida = false;
		if (seguent == this.buffer.length) {
			this.estaPlena = true;
		}
		notifyAll();
	}

	public synchronized String consumir() {
		while (this.estaBuida) {
			try {
				wait();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		seguent--;
		this.estaPlena = false;

		if (seguent == 0) {
			this.estaBuida = true;
		}
		notifyAll();
		return this.buffer[this.seguent];
	}
}
