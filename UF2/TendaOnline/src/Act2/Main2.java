package Act2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main2 {

	public static void main(String[] args) throws InterruptedException {

		BufferB b = new BufferB(10);
		ExecutorService executor = Executors.newFixedThreadPool(4);
		produir(b, executor);
		while (true) {
			Thread.sleep(15000);
			b.clear(10);
			produir(b, executor);
		}

	}

	private static void produir(BufferB b, ExecutorService executor) {

		System.out.println("estic produint");
		executor.execute(new Productor(b));
		executor.execute(new Productor(b));
		executor.execute(new Productor(b));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		executor.execute(new Consumidor(b));
	}

}
