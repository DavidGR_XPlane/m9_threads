package Act2;

public class BufferB {

	private String[] buffer;
	private int seguent;
	private boolean estaBuida;
	private boolean estaPlena;

	public BufferB(int tamany) {
		this.buffer = new String[tamany];
		this.seguent = 0;
		this.estaBuida = true;
		this.estaPlena = false;
	}

	public void clear(int tamany) {
		this.buffer = new String[tamany];
		this.seguent = 0;
		this.estaBuida = true;
		this.estaPlena = false;
		System.out.println("netejant buffer");
	}

	public synchronized void produir(String c) {
		while (this.estaPlena) {
			try {
				wait();
			} catch (Exception e) {

			}
		}
		buffer[seguent] = c;
		seguent++;
		this.estaBuida = false;
		if (seguent == this.buffer.length) {
			this.estaPlena = true;
		}
		notifyAll();
	}

	public synchronized String consumir() {
		while (this.estaBuida) {
			try {
				wait();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		seguent--;
		this.estaPlena = false;
		if (seguent == 0) {
			this.estaBuida = true;
		}
		notifyAll();
		return this.buffer[this.seguent];
	}
}
